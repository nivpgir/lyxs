#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\rightmargin 1cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
אינפי - תרגיל
\family roman
\series medium
\shape up
\size largest
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
3
\end_layout

\begin_layout Author
ניב פגיר -
\family roman
\series medium
\shape up
\size large
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size normal
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
305328692
\end_layout

\begin_layout Enumerate
נראה ראשית כי עבור 
\begin_inset Formula $b\geq0$
\end_inset

 כאשר יש ביטוי מהצורה 
\begin_inset Formula $-b\leq a\leq b$
\end_inset

 מתקיים: 
\begin_inset Formula $\left|a\right|\leq b$
\end_inset

, באמצעות חלוקה למקרים:
\end_layout

\begin_deeper
\begin_layout Enumerate
במקרה ש-
\begin_inset Formula $a\geq0$
\end_inset

 אזי: 
\begin_inset Formula $\left|a\right|=a\implies-b\leq\left|a\right|\leq b$
\end_inset


\end_layout

\begin_layout Enumerate
במקרה ש-
\begin_inset Formula $a\leq0$
\end_inset

 אזי: 
\begin_inset Formula 
\begin{eqnarray*}
a<0 & \implies & -a>0\\
-b\leq a & \implies & -a\leq b
\end{eqnarray*}

\end_inset

ומשילוב האי-שויונות נקבל:
\begin_inset Formula 
\[
b\geq a=\left|-a\right|=\left|a\right|
\]

\end_inset

ובכל אופן:
\begin_inset Formula 
\[
\left|a\right|\leq b
\]

\end_inset

כעת מספיק להוכיח כי, 
\begin_inset Formula $-\left|x-y\right|\leq\left|x\right|-\left|y\right|\leq\left|x-y\right|$
\end_inset

 )כיוון ש-
\begin_inset Formula $\left|x-y\right|\geq0$
\end_inset

(:
\begin_inset Newline newline
\end_inset

נתון אי שויון המשולש הרגיל:
\begin_inset Formula 
\[
\left|a+b\right|\leq\left|a\right|+\left|b\right|
\]

\end_inset

נבחר 
\begin_inset Formula $a=x-y$
\end_inset

 ו-
\begin_inset Formula $b=y$
\end_inset

 ונקבל:
\begin_inset Formula 
\[
\left|x-y+y\right|\leq\left|x-y\right|+\left|y\right|\implies\left|x\right|\leq\left|x-y\right|+\left|y\right|\implies\underline{\left|x\right|-\left|y\right|\leq\left|x-y\right|}
\]

\end_inset

ובשביל לקבל את הצד השני של אי השויון נבחר 
\begin_inset Formula $a=y-x$
\end_inset

 ו-
\begin_inset Formula $b=x$
\end_inset

 ונקבל:
\begin_inset Formula 
\[
\left|y-x+x\right|\leq\left|y-x\right|+\left|x\right|\implies\left|y\right|\leq\left|y-x\right|+\left|x\right|\implies\underline{\left|y\right|-\left|x\right|\leq\left|y-x\right|=\left|x-y\right|}\implies-\left|x-y\right|\leq\left|x\right|-\left|y\right|
\]

\end_inset

נשלב את שתי אי השויונות:
\begin_inset Formula 
\[
-\left|x-y\right|\leq\left|x\right|-\left|y\right|\leq\left|x-y\right|
\]

\end_inset

וכעת מהטענה שהוכחנו בתחילת השאלה ומכך ש
\begin_inset Formula $\left|x-y\right|>0$
\end_inset

 נקבל:
\begin_inset Formula 
\[
\left|\left|x\right|-\left|y\right|\right|\leq\left|x-y\right|
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
תהי 
\begin_inset Formula $\emptyset\neq A\subset\mathbb{F}$
\end_inset

, 
\begin_inset Formula $m\in\mathbb{F}$
\end_inset

 ייקרא חסם תחתון או 
\begin_inset Formula $inf\left(A\right)$
\end_inset

 אם:
\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $m$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset


\end_layout

\begin_layout Enumerate
כל חסם מלרע 
\begin_inset Formula $\tilde{m}$
\end_inset

 של 
\begin_inset Formula $A$
\end_inset

 מקיים: 
\begin_inset Formula $\tilde{m}\leq m$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

צ"ל כי 
\begin_inset Formula $m=inf\left(A\right)$
\end_inset

 אמ"ם:
\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $m$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $\left(\forall\epsilon>0\right)\left(\exists a\in A\right)\left(a<m+\epsilon\right)$
\end_inset


\begin_inset Newline newline
\end_inset

הוכחה:
\begin_inset Newline newline
\end_inset


\begin_inset Formula $\implies$
\end_inset

 נניח ש
\begin_inset Formula $m$
\end_inset

 חסם תחתון, נוכיח שמתקיימות
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
התכונות:
\begin_inset Newline newline
\end_inset

מהגדרת החסם התחתון תכונה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
מתקיימת.
\begin_inset Newline newline
\end_inset

נוכיח את תכונה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\numeric off
:
\begin_inset Newline newline
\end_inset

נניח בשלילה כי 
\begin_inset Formula $\left(\exists\epsilon>0\right)\left(\forall a\in A\right)\left(a\geq m+\epsilon\right)$
\end_inset

:
\begin_inset Newline newline
\end_inset

נגדיר:
\begin_inset Formula 
\[
m+\epsilon=\tilde{m}
\]

\end_inset

ונקבל כי 
\begin_inset Formula 
\[
\left(\forall a\in A\right)\left(\tilde{m}\leq a\right)
\]

\end_inset

כלומר 
\begin_inset Formula $\tilde{m}$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset

 ובנוסף:
\begin_inset Formula 
\[
\tilde{m}=m+\epsilon>m
\]

\end_inset

בסתירה לתכונה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
של 
\begin_inset Formula $inf\left(A\right)$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\impliedby$
\end_inset

נניח כי שתי התכונות מתקיימות, ונוכיח ש-
\begin_inset Formula $m$
\end_inset

 חסם תחתון:
\begin_inset Newline newline
\end_inset

ראשית כל נתון כי 
\begin_inset Formula $m$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset

 ולכן תנאי
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
מתקיים
\begin_inset Newline newline
\end_inset

נותר להוכיח כי לכל חסם מלרע 
\begin_inset Formula $\tilde{m}$
\end_inset

 של 
\begin_inset Formula $A$
\end_inset

 מתקיים: 
\begin_inset Formula $\tilde{m}\leq m$
\end_inset

:
\begin_inset Newline newline
\end_inset

נניח בשלילה כי קיים חסם מלרע 
\begin_inset Formula $\tilde{m}$
\end_inset

 כך ש-
\begin_inset Formula $\tilde{m}>m$
\end_inset

, נשים לב כי:
\begin_inset Formula 
\[
\tilde{m}=\left(\tilde{m}-m\right)+m
\]

\end_inset

כיוון ש-
\begin_inset Formula $\tilde{m}>m\implies\tilde{m}-m>0$
\end_inset

, מתכונה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
עבור כל 
\begin_inset Formula $\epsilon>0$
\end_inset

 התכונה מתקיימת, ובפרט עבור 
\begin_inset Formula $\epsilon_{0}=\tilde{m}-m$
\end_inset

 קיים:
\begin_inset Formula 
\[
\left(\exists a\in A\right)\left(a<m+\epsilon_{0}=m+\tilde{m}-m=\tilde{m}\right)
\]

\end_inset

 בסתירה לכך ש-
\begin_inset Formula $\tilde{m}$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset


\begin_inset Formula $\blacksquare$
\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
לא נכון, דוגמא נגדית: 
\begin_inset Formula $A=\left\{ 1,2\right\} $
\end_inset

, 
\begin_inset Formula $A^{2}=\left\{ 1,4\right\} $
\end_inset

, 
\begin_inset Formula $A\cdot A=\left\{ 1,2,4\right\} $
\end_inset


\end_layout

\begin_layout Enumerate
לא נכון, דוגמא נגדית: 
\begin_inset Formula $A=\left\{ a\mid n\in\mathbb{N},a=-n\right\} $
\end_inset

, 
\begin_inset Formula $A^{2}=\left\{ b\mid n\in\mathbb{N},b=\left(-n\right)^{2}=n^{2}\right\} $
\end_inset

, נשים לב שזו קבוצה לא חסומה מלעיל )מארכימדיות 
\begin_inset Formula $\mathbb{N}$
\end_inset

( בעוד ש-
\begin_inset Formula $A$
\end_inset

 חסומה מלעיל ע"י
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 כל מספר אי שלילי
\end_layout

\begin_layout Enumerate
תהי 
\begin_inset Formula $A$
\end_inset

 קבוצה חסומה, אזי קיימים 
\begin_inset Formula $s,m\in\mathbb{F}$
\end_inset

 כך ש-
\begin_inset Formula $m=inf\left(a\right)\wedge s=sup\left(A\right)$
\end_inset

, אזי 
\begin_inset Formula $\forall a\in A$
\end_inset

:
\begin_inset Formula 
\[
m\leq a\leq s
\]

\end_inset

אזי קיימים שלושה מקרים:
\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $0\leq m\leq s$
\end_inset

, במקרה זה מותר להעלות בחזקת
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
את כל אי השויון ונקבל כי עבור 
\begin_inset Formula $a\in A$
\end_inset

, לכל 
\begin_inset Formula $a^{2}=b\in A^{2}$
\end_inset

:
\begin_inset Formula 
\[
m\leq a\leq s\implies m^{2}\leq b\leq s^{2}
\]

\end_inset

ומכאן ש-
\begin_inset Formula $A^{2}$
\end_inset

 חסומה.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $0\geq m\geq s$
\end_inset

, ניתן להעלות בריבוע ולהפוך את סימן היחס ונקבל כי עבור 
\begin_inset Formula $a\in A$
\end_inset

, לכל 
\begin_inset Formula $a^{2}=b\in A^{2}$
\end_inset

:
\begin_inset Formula 
\[
m\leq a\leq s\implies m^{2}\geq b\geq s^{2}
\]

\end_inset

ומכאן ש-
\begin_inset Formula $A^{2}$
\end_inset

 חסומה.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $m\leq0\leq s$
\end_inset

, הוכחנו בכיתה כי 
\begin_inset Formula $\left(\forall a\in\mathbb{F}\right)\left(a^{2}\geq0\right)$
\end_inset

 וכיוון ש
\begin_inset Formula $A\subset\mathbb{F}$
\end_inset

 מתקיים בפרט ש-
\begin_inset Formula $\left(\forall a\in A\right)\left(a^{2}\geq0\right)$
\end_inset

, ולכן
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit

\begin_inset Formula $0$
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
הוא חסם מלרע של 
\begin_inset Formula $A^{2}$
\end_inset

, נשים לב כי עבור כל 
\begin_inset Formula $A\ni a>0$
\end_inset

:
\begin_inset Formula 
\[
0\leq a\leq s\implies\leq a^{2}\leq s^{2}
\]

\end_inset

ובנוסף כי עבור כל 
\begin_inset Formula $A\ni a<0$
\end_inset

:
\begin_inset Formula 
\[
0\geq a\geq m\implies0\leq a^{2}\leq m^{2}
\]

\end_inset

כעת רק נותר לבחור כחסם מלרע את 
\begin_inset Formula $max\left(s^{2},m^{2}\right)$
\end_inset

 ונראה כי עבור כל 
\begin_inset Formula $a^{2}=b\in A^{2}$
\end_inset

 מתקיים:
\begin_inset Formula 
\[
0\leq b\leq max\left(s^{2},m^{2}\right)
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
לא נכון, דוגמא נגדית:
\begin_inset Formula $A=\left\{ 1,2,3,4\right\} \implies4=max\left(A\right)$
\end_inset

, אך: 
\begin_inset Formula $sup\left(A\backslash\left\{ 4\right\} \right)=sup\left(\left\{ 1,2,3\right\} \right)=3$
\end_inset


\end_layout

\begin_layout Enumerate
נכון, 
\begin_inset Formula $s=sup\left(A\right)\implies\left(\forall a\in\left(s\geq a\right)\right)$
\end_inset

, בנוסף נתון כי 
\begin_inset Formula $s\in A$
\end_inset

, נשים לב כי אלה בדיוק התנאים הדרושים לכך ש-
\begin_inset Formula $s=max\left(A\right)$
\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
יהי 
\begin_inset Formula $b\in B$
\end_inset

,
\begin_inset Formula $B\subset A\implies b\in A$
\end_inset

, כיוון ש-
\begin_inset Formula $A$
\end_inset

 חסומה מלעיל 
\begin_inset Formula $\left(\exists s\in\mathbb{F}\right)\left(\forall a\in A\right)\left(s\geq a\right)$
\end_inset

, בפרט עבור 
\begin_inset Formula $b$
\end_inset

 מתקיים 
\begin_inset Formula $s\geq b$
\end_inset

, כלומר 
\begin_inset Formula $\left(\forall b\in B\right)\left(b\leq s\right)$
\end_inset

 ולכן 
\begin_inset Formula $b$
\end_inset

 חסומה.
\end_layout

\begin_layout Enumerate
לא נכון, דוגמא נגדית: 
\begin_inset Formula $A=-\mathbb{N}$
\end_inset

 ו-
\begin_inset Formula $B=\left\{ -1\right\} $
\end_inset

, אזי 
\begin_inset Formula $B\subset A$
\end_inset

 וגם 
\begin_inset Formula $B$
\end_inset

 חסומה מלרע, אך 
\begin_inset Formula $A$
\end_inset

 לא חסומה מארכימדיות הטבעיים.
\end_layout

\begin_layout Enumerate
לא נכון, דוגמא נגדית: 
\begin_inset Formula $A=\left[0,1\right]$
\end_inset

 ו-
\begin_inset Formula $B=(0,1]$
\end_inset

, נשים לב כי 
\begin_inset Formula $B\neq A$
\end_inset

 אך 
\begin_inset Formula $sup\left(A\right)=sup\left(B\right)=1$
\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
נתונה הקבוצה:
\begin_inset Formula 
\[
A=\left\{ x^{2}+5x-14\mid x\in\mathbb{F}\right\} 
\]

\end_inset

נשים לב כי 
\begin_inset Formula $x^{2}+5x-14=\left(x+7\right)\left(x-2\right)$
\end_inset

, לכן, אם 
\begin_inset Formula $x>3$
\end_inset

 אז 
\begin_inset Formula $x-2>1$
\end_inset

 ולכן 
\begin_inset Formula $x^{2}+5x-14>x+7>x$
\end_inset

.
 נניח בשלילה כי 
\begin_inset Formula $M$
\end_inset

 חסם מלעיל של 
\begin_inset Formula $A$
\end_inset

.
 אם 
\begin_inset Formula $M\leq3$
\end_inset

 אז ניקח 
\begin_inset Formula $x=3$
\end_inset

ונקבל:
\begin_inset Formula 
\[
3^{2}+5\cdot3-14=10>3\geq M
\]

\end_inset

בסתירה לכך ש-
\begin_inset Formula $M$
\end_inset

 חסם מלעיל של 
\begin_inset Formula $A$
\end_inset

.
 אחרת 
\begin_inset Formula $M>3$
\end_inset

, ונקבל כי 
\begin_inset Formula $M^{2}+5M-14>M$
\end_inset

 וזו שוב סתירה לכך ש-
\begin_inset Formula $M$
\end_inset

 הוא חסם מלעיל של 
\begin_inset Formula $A$
\end_inset

, כלומר ל-
\begin_inset Formula $A$
\end_inset

 אין חסם מלעיל.
\begin_inset Newline newline
\end_inset

נבדוק חסם מלרע: נשתמש בשיטת ההשלמה בריבוע ונקבל כי: 
\begin_inset Formula 
\[
x^{2}+5x-14=\left(x+2.5\right)^{2}-6.25-14=\left(x+2.5\right)^{2}-20.25
\]

\end_inset

אנו יודעים כי
\begin_inset Formula 
\[
\left(\forall x\in\mathbb{F}\right)\left(\left(x+2.5\right)^{2}\geq0\implies\left(x+2.5\right)^{2}-20.25\geq-20.25\implies x^{2}+5x-14\geq-20.25\right)
\]

\end_inset

 ולכן 
\begin_inset Formula $-20.25$
\end_inset

 הוא חסם מלרע של 
\begin_inset Formula $A$
\end_inset

, נשים לב כי 
\begin_inset Formula $-20.25$
\end_inset

 שייך ל-
\begin_inset Formula $A$
\end_inset

 שכן עבור 
\begin_inset Formula $x=-2.5$
\end_inset

, נקבל:
\begin_inset Formula 
\[
x^{2}+5x-14=\left(-2.5\right)^{2}+5\cdot\left(-2.5\right)-14=-20.25
\]

\end_inset

 ולכן 
\begin_inset Formula $min\left(A\right)=-20.25$
\end_inset


\end_layout

\begin_layout Enumerate
נתונה הקבוצה:
\begin_inset Formula 
\[
A=\left\{ x\in\mathbb{F}\mid x^{2}+5x-14>0\right\} 
\]

\end_inset

נשים לב כי: 
\begin_inset Formula 
\[
x^{2}+5x-14=\left(x+2.5\right)^{2}-20.25>0\iff\left(x+2.5\right)^{2}>20.25
\]

\end_inset


\begin_inset Formula 
\[
\iff\left(x+2.5>4.5\right)\vee\left(x+2.5<-4.5\right)\iff\left(x>2\right)\vee\left(x<-7\right)
\]

\end_inset

נניח 
\begin_inset Formula $M>3$
\end_inset

 חסם מלרע אזי נבחר 
\begin_inset Formula $x=M-1$
\end_inset

 ונקבל כי:
\begin_inset Formula 
\[
3<M\implies2<M-1\implies M-1\in A
\]

\end_inset

בסתירה לכך ש-
\begin_inset Formula $M$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset

.
 
\begin_inset Newline newline
\end_inset

נניח 
\begin_inset Formula $M\leq3$
\end_inset

 חסם מלרע אזי נבחר 
\begin_inset Formula $x=M-11$
\end_inset

 ונקבל:
\begin_inset Formula 
\[
3\geq M\implies M-11\leq-8\implies M-11\in A
\]

\end_inset

בסתירה לכך ש-
\begin_inset Formula $M$
\end_inset

 חסם מלרע של 
\begin_inset Formula $A$
\end_inset

.
\begin_inset Newline newline
\end_inset

עבור חסם מלעיל, נניח כי 
\begin_inset Formula $M>-9$
\end_inset

 אזי נבחר 
\begin_inset Formula $x=M+11$
\end_inset

 ונקבל:
\begin_inset Formula 
\[
-9>M\implies2<M+11\implies M+11\in A
\]

\end_inset

בסתירה לכך ש-
\begin_inset Formula $M$
\end_inset

 חסם מלעיל של 
\begin_inset Formula $A$
\end_inset

.
 עבור 
\begin_inset Formula $M\leq-9$
\end_inset

, נבחר 
\begin_inset Formula $x=M+1$
\end_inset

 ונקבל:
\begin_inset Formula 
\[
-9\geq M\implies-8\geq M+1\implies M+1\in A
\]

\end_inset

בסתירה לכך ש-
\begin_inset Formula $M$
\end_inset

 חסם מלעיל של 
\begin_inset Formula $A$
\end_inset

.
\end_layout

\begin_layout Enumerate
נתונה הקבוצה:
\begin_inset Formula 
\[
A=\left\{ x\in\mathbb{F}\mid x^{2}+5x-14\leq0\right\} 
\]

\end_inset


\begin_inset Formula 
\[
x^{2}+5x-14=\left(x+2.5\right)^{2}-20.25\leq0\iff\left(x+2.5\right)^{2}\leq20.25
\]

\end_inset


\begin_inset Formula 
\[
\iff\left(x+2.5\leq4.5\right)\vee\left(x+2.5\geq-4.5\right)\iff\left(x\leq2\right)\vee\left(x\geq-7\right)\iff\underline{-7\leq x\leq2}
\]

\end_inset

הראינו כי התנאי לשייכות לקבוצה 
\begin_inset Formula $A$
\end_inset

 הוא שקול ל:
\begin_inset Formula 
\[
-7\leq x\leq2
\]

\end_inset

ולכן 
\begin_inset Formula $A=\left\{ x\in\mathbb{F}\mid-7\leq x\leq2\right\} $
\end_inset

.
\begin_inset Newline newline
\end_inset

לכל 
\begin_inset Formula $a\in A$
\end_inset

, 
\begin_inset Formula $a\leq2$
\end_inset

, ולכן
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
חסם מלעיל לקבוצה, בנוסף 
\begin_inset Formula $2\in A$
\end_inset

 ולכן
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit

\begin_inset Formula $2=max\left(A\right)$
\end_inset


\begin_inset Newline newline
\end_inset

לכל 
\begin_inset Formula $a\in A$
\end_inset

, 
\begin_inset Formula $a\geq-7$
\end_inset

, ולכן 
\begin_inset Formula $-7$
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
חסם מלרע לקבוצה, בנוסף 
\begin_inset Formula $-7\in A$
\end_inset

 ולכן
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit

\begin_inset Formula $-7=min\left(A\right)$
\end_inset


\end_layout

\begin_layout Enumerate
נתונה הקבוצה:
\begin_inset Formula 
\[
A=\left\{ x^{2}+5x-14\mid x\in\mathbb{F},\left|x\right|\leq10\right\} =\left\{ x^{2}+5x-14\mid x\in\mathbb{F},-10\leq x\leq10\right\} 
\]

\end_inset

נשים לב כי קבוצה זו הינה תת קבוצה של הקבוצה מסעיף א' )שכן בסעיף א' היא כללה
 את כל האיברים שמקיימים 
\begin_inset Formula $x\in\mathbb{F}$
\end_inset

 ואילו כאן היא מוגבלת לאיברים 
\begin_inset Formula $x\in\mathbb{F}$
\end_inset

, שעבורם 
\begin_inset Formula $-10<x<10$
\end_inset

.
\begin_inset Newline newline
\end_inset

חסם מלרע: בסעיף א' הראינו כי קיים חסם מלרע לקבוצה כאשר 
\begin_inset Formula $x=2.5$
\end_inset

, וכיוון שזו תת קבוצה של הקבוצה מסעיף א' גם כאן חסם זה יהיה קיים, וערכו:
 
\begin_inset Formula $-20.25$
\end_inset

, וכפי שהראינו בסעיף א' איבר זה שייך ל-
\begin_inset Formula $A$
\end_inset

 ולכן הוא 
\begin_inset Formula $min\left(A\right)$
\end_inset

.
 
\begin_inset Newline newline
\end_inset

חסם מלעיל: בסעיף א' הראינו כי כאשר 
\begin_inset Formula $x>2$
\end_inset

 איברי 
\begin_inset Formula $A$
\end_inset

 גדלים, בקבוצה זו האיבר הגדול ביותר יהיה כאשר 
\begin_inset Formula $x=10$
\end_inset

 נקרא לאיבר זה 
\begin_inset Formula $m=10^{2}+50-14=136$
\end_inset

, נשים לב גם כי עבור כל איבר אחר ב-
\begin_inset Formula $a$
\end_inset

 
\begin_inset Formula $m>a$
\end_inset

 ועל כן איבר זה הוא 
\begin_inset Formula $\sup\left(A\right)$
\end_inset

 אך איבר זה לא שייך ל-
\begin_inset Formula $A$
\end_inset

 ובהנחה שזהו שדה צפוף )הנחה שאני הנחתי מאחר שעניין זה לא הוגדר כראוי בתרגיל(
 אזי קיים 
\begin_inset Formula $y$
\end_inset

 כך ש-
\begin_inset Formula $x<y<136$
\end_inset

 ולכן לא קיים מקסימום לקבוצה.
\end_layout

\end_deeper
\end_body
\end_document
