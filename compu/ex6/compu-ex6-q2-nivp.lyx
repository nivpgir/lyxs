#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\rightmargin 1cm
\secnumdepth -1
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation 0bp
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
חישוביות - תרגיל
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
6
\end_layout

\begin_layout Author
ניב פגיר -
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
305328692
\end_layout

\begin_layout Section

\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\end_layout

\begin_layout Subsection
סעיף א'
\end_layout

\begin_layout Standard
\begin_inset Formula $f$
\end_inset

 חשיבה, ולכן קיימת מ"ט 
\begin_inset Formula $F$
\end_inset

 שמחשבת את 
\begin_inset Formula $f$
\end_inset

.
\end_layout

\begin_layout Standard
בנוסף, 
\begin_inset Formula $L\in RE$
\end_inset

 ולכן קיים אנומרטור 
\begin_inset Formula $E$
\end_inset

 כך ש 
\begin_inset Formula $L\left(E\right)=L$
\end_inset

.
\end_layout

\begin_layout Standard
נבנה מכונה 
\begin_inset Formula $M_{f}$
\end_inset

 באמצעות 
\begin_inset Formula $E$
\end_inset

 ו
\begin_inset Formula $F$
\end_inset

 שמזהה את 
\begin_inset Formula $f\left(L\right)$
\end_inset

, 
\begin_inset Formula $M_{f}$
\end_inset

 תוגדר כך:
\end_layout

\begin_layout Enumerate
\begin_inset Formula $M_{f}$
\end_inset

 תקבל את 
\begin_inset Formula $w$
\end_inset

.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $M_{f}$
\end_inset

 תתחיל להריץ את 
\begin_inset Formula $E$
\end_inset

.
 ולכל מילה ש
\begin_inset Formula $E$
\end_inset

 מדפיסה:
\end_layout

\begin_deeper
\begin_layout Enumerate
תעתיק את 
\begin_inset Formula $w$
\end_inset

 מהסרט של 
\begin_inset Formula $E$
\end_inset

 לסרט שלה.
\end_layout

\begin_layout Enumerate
תריץ את 
\begin_inset Formula $F$
\end_inset

 על 
\begin_inset Formula $w$
\end_inset

, ותקבל על הסרט את 
\begin_inset Formula $w^{\prime}$
\end_inset

.
\end_layout

\begin_layout Enumerate
אם 
\begin_inset Formula $w^{\prime}=w$
\end_inset

, תעבור ל
\begin_inset Formula $q_{acc}$
\end_inset

.
 אחרת תמשיך למילה הבאה של 
\begin_inset Formula $E$
\end_inset

.
\end_layout

\end_deeper
\begin_layout Standard
כעת, צריך להוכיח שאם 
\begin_inset Formula $w\in f\left(L\right)$
\end_inset

 אז 
\begin_inset Formula $M_{f}\left(w\right)=acc$
\end_inset

.
\end_layout

\begin_layout Standard
יהי 
\begin_inset Formula $w\in f\left(L\right)$
\end_inset

, נסמן ב
\begin_inset Formula $x_{i}$
\end_inset

 את המילה ה
\begin_inset Formula $i$
\end_inset

 ש
\begin_inset Formula $E$
\end_inset

 מדפיסה.
 כיוון ש
\begin_inset Formula $w\in f\left(L\right)$
\end_inset

 קיים 
\begin_inset Formula $i$
\end_inset

 כך ש
\begin_inset Formula $f\left(x_{i}\right)=w$
\end_inset

.
 אז 
\begin_inset Formula $M_{f}$
\end_inset

 מריצה מילים של 
\begin_inset Formula $E$
\end_inset

, וכאשר 
\begin_inset Formula $E$
\end_inset

 מדפיסה את 
\begin_inset Formula $x_{i}$
\end_inset

, 
\begin_inset Formula $M_{f}$
\end_inset

 תבדוק האם 
\begin_inset Formula $f\left(x_{i}\right)=w$
\end_inset

, וכיוון שתנאי זה יתקיים, תעבור ל
\begin_inset Formula $q_{acc}$
\end_inset

 ותחזיר 
\begin_inset Formula $acc$
\end_inset

.
\end_layout

\begin_layout Subsection
סעיף ב'
\end_layout

\begin_layout Standard
\begin_inset Formula $f$
\end_inset

 חשיבה, ולכן קיימת מ"ט 
\begin_inset Formula $F$
\end_inset

 שמחשבת את 
\begin_inset Formula $f$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $L\in Re$
\end_inset

 ולכן קיימת מ"ט 
\begin_inset Formula $M$
\end_inset

 שלכל 
\begin_inset Formula $w\in L$
\end_inset

, 
\begin_inset Formula $M\left(w\right)=acc$
\end_inset

.
\end_layout

\begin_layout Standard
נגדיר מכונה 
\begin_inset Formula $M_{f^{-1}}$
\end_inset

 באמצעות 
\begin_inset Formula $F$
\end_inset

 ו
\begin_inset Formula $M$
\end_inset

 אשר 
\begin_inset Formula $M_{f^{-1}}$
\end_inset

 הקבלת מילה 
\begin_inset Formula $w$
\end_inset

, תבצע את הפעולות הבאות:
\end_layout

\begin_layout Enumerate
תיצור עוד עותק של 
\begin_inset Formula $w$
\end_inset

 על הסרט.
\end_layout

\begin_layout Enumerate
תריץ את 
\begin_inset Formula $F$
\end_inset

 על העותק של 
\begin_inset Formula $w$
\end_inset

 ותקבל 
\begin_inset Formula $w^{\prime}$
\end_inset

.
\end_layout

\begin_layout Enumerate
תריץ את 
\begin_inset Formula $M$
\end_inset

 על 
\begin_inset Formula $w^{\prime}$
\end_inset

, ואם 
\begin_inset Formula $M$
\end_inset

 עצרה על 
\begin_inset Formula $q_{acc}$
\end_inset

, 
\begin_inset Formula $M_{f^{-1}}$
\end_inset

 תעבור ל
\begin_inset Formula $q_{acc}$
\end_inset

 ותחזיר 
\begin_inset Formula $acc$
\end_inset

.
 אם 
\begin_inset Formula $M$
\end_inset

 עצרה על 
\begin_inset Formula $q_{rej}$
\end_inset

 
\begin_inset Formula $M_{f^{-1}}$
\end_inset

 תעבור ל
\begin_inset Formula $q_{rej}$
\end_inset

 ותחזיר 
\begin_inset Formula $rej$
\end_inset

.
\end_layout

\begin_layout Standard
למעשה בהינתן מילה 
\begin_inset Formula $w$
\end_inset

, 
\begin_inset Formula $M_{f^{-1}}$
\end_inset

 מחזירה את הפלט של 
\begin_inset Formula $M\left(f\left(w\right)\right)$
\end_inset

, ומההגדרה:
\begin_inset Formula 
\[
y\in f^{-1}\left(L\right)\iff f\left(y\right)\in L\iff M\left(f\left(y\right)\right)=acc
\]

\end_inset


\end_layout

\end_body
\end_document
