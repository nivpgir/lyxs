#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\rightmargin 1cm
\secnumdepth -1
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation 0bp
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
חישוביות – תרגיל
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
8
\end_layout

\begin_layout Author
ניב פגיר -
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
305328692
\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "../../ProperSetup.lyx"

\end_inset


\end_layout

\begin_layout Section
שאלה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
5
\end_layout

\begin_layout Subsection
סעיף א'
\end_layout

\begin_layout Standard
\begin_inset Formula $N$
\end_inset

 תפעל בצורה הבאה:
\end_layout

\begin_layout Enumerate
\begin_inset Formula $N$
\end_inset

 תבחר תת קבוצת קודקודים של 
\begin_inset Formula $G$
\end_inset

 ותסמנה 
\begin_inset Formula $S$
\end_inset

 )למשל ע"י מעבר על כל קודקודי הגרף ובכל פעם תבחר האם לקחת את הקודקוד לתת
 הקבוצה הנבדקת או לא לקחת ניתן לעשות בחירה מסויימת שכזו ב
\begin_inset Formula $\left(O\left(n\right)\right)$
\end_inset

( 
\end_layout

\begin_layout Enumerate
אם 
\begin_inset Formula $\left|S\right|\neq k$
\end_inset

, תעצור ותחזיר 
\begin_inset Formula $rej$
\end_inset

.
\end_layout

\begin_layout Enumerate
לכל 
\begin_inset Formula $v\in S$
\end_inset

:
\end_layout

\begin_deeper
\begin_layout Enumerate
לכל 
\begin_inset Formula $u\in S$
\end_inset

:
\end_layout

\begin_deeper
\begin_layout Enumerate
אם 
\begin_inset Formula $u,v$
\end_inset

 שכנים, תעצור ותחזיר 
\begin_inset Formula $rej$
\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
תעצור ותחזיר 
\begin_inset Formula $acc$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $N$
\end_inset

 עושה כמות סופית של פעולות בכל שלב )בחירת תת קבוצה של קודקודים 
\begin_inset Formula $O\left(n\right)$
\end_inset

, ומעבר על כל זוג קודקודים בקבוצה 
\begin_inset Formula $O\left(n^{2}\right)$
\end_inset

(, ובכל מקרה מסיימת את ריצתה, לכן היא תעצור על כל מילה.
\end_layout

\begin_layout Standard
אם 
\begin_inset Formula $\left\langle G,k\right\rangle \in IS$
\end_inset

, אז קיימת תת קבוצה 
\begin_inset Formula $S$
\end_inset

 בגודל 
\begin_inset Formula $k$
\end_inset

, כך שלכל זוג קודקודים בה, אין צלע ביניהן, אז קיים ענף, בעץ הבחירות של 
\begin_inset Formula $N$
\end_inset

 שבוחר את 
\begin_inset Formula $S$
\end_inset

, לאחר שהוא יבחר את 
\begin_inset Formula $S$
\end_inset

 הוא יעבור על כל זוגות הקודקודים בה, ולכל זוג, התנאי לעצירה והחזרת 
\begin_inset Formula $rej$
\end_inset

 לא יתקיים, לכן היא 
\begin_inset Formula $N$
\end_inset

 צעבור על כל הזוגות בהצלחה, תעצור ותקבל את המילה.
\end_layout

\begin_layout Standard
אם 
\begin_inset Formula $\left\langle G,k\right\rangle \notin IS$
\end_inset

, אז בכל תת קבוצה בגודל 
\begin_inset Formula $k$
\end_inset

 יש לפחות זוג צלעות 
\begin_inset Formula $u,v$
\end_inset

 שיש ביניהן צלע, לכן לכל בחירה של 
\begin_inset Formula $S$
\end_inset

 בגודל 
\begin_inset Formula $k$
\end_inset

, 
\begin_inset Formula $N$
\end_inset

 תגיע לזוג הקודקודים הללו, וכשתבדוק האם הם שכנים, תגלה שכן, תעצור ותחזיר
 
\begin_inset Formula $rej$
\end_inset

, ולכן 
\begin_inset Formula $\left\langle G,k\right\rangle \notin L\left(N\right)$
\end_inset

.
\end_layout

\begin_layout Standard
קיבלנו כי 
\begin_inset Formula $N$
\end_inset

 עוצרת על כל קלט, וגם 
\begin_inset Formula $w\in L\left(N\right)\iff w\in IS$
\end_inset

, לכן 
\begin_inset Formula $N$
\end_inset

 מכריעה את 
\begin_inset Formula $IS$
\end_inset

.
\end_layout

\begin_layout Standard
בנוסף, אורך הריצה הארוכה ביותר של 
\begin_inset Formula $N$
\end_inset

 הוא כאשר היא תבחר את כל הקודקודים ב
\begin_inset Formula $G$
\end_inset

, ז"א 
\begin_inset Formula $\left|S\right|=n$
\end_inset

, ואז זמן הריצה יהיה:
\begin_inset Formula 
\[
O\left(n\right)+O\left(n^{2}\right)=O\left(n^{2}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
לכן 
\begin_inset Formula $IS\in NTIME\left(n^{2}\right)$
\end_inset

, וקיבלנו כי 
\begin_inset Formula $IS\in NP$
\end_inset

.
\end_layout

\begin_layout Subsection
סעיף ב'
\end_layout

\begin_layout Standard
נגדיר מוודא 
\begin_inset Formula $V$
\end_inset

 שמקבל קלט של גרף 
\begin_inset Formula $G$
\end_inset

, מספר שלם 
\begin_inset Formula $k$
\end_inset

, וקבוצת קודקודים 
\begin_inset Formula $S$
\end_inset

 
\begin_inset Formula $\left\langle G,k,S\right\rangle $
\end_inset

, ובודק האם 
\begin_inset Formula $S$
\end_inset

 היא קבוצת קודקודים בגודל 
\begin_inset Formula $k$
\end_inset

, כך שאין צלע בין אף זוג קודקודים ב
\begin_inset Formula $S$
\end_inset

, האלגוריתם שתיארנו בסעיף קודם מבצע זאת, בזמן של 
\begin_inset Formula $O\left(k^{2}\right)$
\end_inset

, הזמן שלוקח לעבור על כל זוג צלעות ב
\begin_inset Formula $S$
\end_inset

.
 
\end_layout

\begin_layout Standard
הנכונות והעצירה של 
\begin_inset Formula $V$
\end_inset

 נובעת ישירות מההגדרות בצורה דומה לסעיף קודם.
\end_layout

\begin_layout Section

\end_layout

\end_body
\end_document
