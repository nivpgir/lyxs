#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\rightmargin 1cm
\secnumdepth -1
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation 0bp
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
הסתברות - תרגיל
\family roman
\series medium
\shape up
\size largest
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\end_layout

\begin_layout Author
ניב פגיר -
\family roman
\series medium
\shape up
\size large
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
305328692
\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\numeric off
 
\end_layout

\begin_layout Standard
יהי 
\begin_inset Formula $\left(\Omega,\mathcal{F}\right)$
\end_inset

 מרחב מדיד, עבור 
\begin_inset Formula $\Omega=\mathbb{N}$
\end_inset

 מרחב מדגים וסיגמא אלגברה: 
\begin_inset Formula $\mathcal{F}=2^{\Omega}$
\end_inset

 
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
א( תהי 
\begin_inset Formula $P:\mathcal{F}\rightarrow\left[0,1\right]$
\end_inset

 פונקציה המקיימת לכל 
\begin_inset Formula $A,B\in\mathcal{F}$
\end_inset

: 
\begin_inset Formula 
\[
\left|A\right|=\left|B\right|\implies P\left(A\right)=P\left(B\right)
\]

\end_inset

הראה ש-
\begin_inset Formula $P$
\end_inset

 אינה פונקציית הסתברות.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 יהי 
\begin_inset Formula $m\in\mathbb{R}$
\end_inset

, ותהיה 
\begin_inset Formula $\left(A_{i}\right)_{i=1}^{\infty}$
\end_inset

 סדרת קבוצות המוכלות ב-
\begin_inset Formula $\Omega$
\end_inset

 המוגדרת ע"י: 
\begin_inset Formula 
\[
\forall i\in\mathbb{N}\ \left|A_{i}\right|=\left\{ k\mid im\leq k<i\left(m+1\right)\right\} 
\]

\end_inset

אז לכל 
\begin_inset Formula $i,k$
\end_inset

 
\begin_inset Formula $\left|A_{i}\right|=\left|A_{k}\right|$
\end_inset

 ובנוסף 
\begin_inset Formula $A_{i}\cap A_{k}=\phi$
\end_inset

, נסמן לכל 
\begin_inset Formula $i\in\mathbb{N}$
\end_inset

: 
\begin_inset Formula $P\left(A_{i}\right)=x$
\end_inset

,
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 בנוסף, האיחוד האינסופי של כל איברי הסדרה ייתן לנו את 
\begin_inset Formula $\Omega$
\end_inset

: 
\begin_inset Formula 
\[
\bigcup_{i=1}^{\infty}A_{i}=\Omega
\]

\end_inset

וכיוון שכל איברי הסדרה זרים יתקיים:
\begin_inset Formula 
\[
P\left(\bigcup_{i=1}^{\infty}A_{i}\right)=\sum_{i=1}^{\infty}P\left(A_{i}\right)=\sum_{i=1}^{\infty}x
\]

\end_inset

זהו טור מתבדר, ולכן אנו יודעים שקיים 
\begin_inset Formula $N$
\end_inset

 כך ש-
\begin_inset Formula $\sum_{i=1}^{N}x>1$
\end_inset

, ולכן:
\begin_inset Formula 
\[
P\left(\bigcup_{i=1}^{\infty}A_{i}\right)=\sum_{i=1}^{\infty}P\left(A_{i}\right)=\sum_{i=1}^{\infty}x=\sum_{i=N+1}^{\infty}x+\sum_{i=1}^{N}x>\sum_{i=1}^{N}x>1
\]

\end_inset

 אך מצד שני: 
\begin_inset Formula 
\[
P\left(\bigcup_{i=1}^{\infty}A_{i}\right)=P\left(\Omega\right)=1
\]

\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 וקיבלנו סתירה.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
ב( נגדיר 
\begin_inset Formula $P:\mathcal{F}\rightarrow\left[0,1\right]$
\end_inset

 כך:
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 לכל יחידון 
\begin_inset Formula $\left\{ n\right\} \in\mathcal{F}$
\end_inset

 נגדיר:
\begin_inset Formula 
\[
P\left(\left\{ n\right\} \right)=\frac{1}{2^{n+1}}
\]

\end_inset

ולכל 
\begin_inset Formula $\omega\in\mathcal{F}$
\end_inset

 כך ש-
\begin_inset Formula $\left|\omega\right|>1$
\end_inset

נגדיר:
\begin_inset Formula 
\[
P\left(\omega\right)=\sum_{n\in\omega}P\left(\left\{ n\right\} \right)
\]

\end_inset


\begin_inset Newline newline
\end_inset

נותר רק להראות שמתקיימים
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
התנאים:
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\numeric off
( אי שליליות: 
\begin_inset Formula 
\[
\forall n\in\mathbb{N}\ \frac{1}{2^{n+1}}\geq0\implies\forall\omega\in\mathcal{F}\ P\left(\omega\right)\geq0
\]

\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\numeric off
(
\begin_inset Formula $P\left(\Omega\right)=1$
\end_inset

:
\begin_inset Formula 
\[
P\left(\Omega\right)=\sum_{n\in\Omega}P\left(\left\{ n\right\} \right)=\sum_{n\in\mathbb{N}}\frac{1}{2^{n+1}}=\sum_{n=1}^{\infty}\frac{1}{2^{n+1}}\stackrel{(*)}{=}1
\]

\end_inset

 )*( זהו טור מתכנס שגבולו הוא
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\numeric off
, כפי שהוכחנו בקורסי אינפי.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
3
\numeric off
( תהי 
\begin_inset Formula $\left(A_{n}\right)_{n=1}^{\infty}$
\end_inset

 סדרה אינסופית של קבוצות זרות מתוך 
\begin_inset Formula $\Omega$
\end_inset

, אזי:
\begin_inset Formula 
\[
P\left(\bigcup_{i=1}^{\infty}A_{i}\right)=\sum_{i\in\bigcup_{i=1}^{\infty}A_{i}}\frac{1}{2^{i+1}}=\sum_{i=1}^{\infty}\left(\sum_{j\in A_{i}}\frac{1}{2^{j+1}}\right)=\sum_{i=1}^{\infty}P\left(A_{i}\right)
\]

\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 הראינו שכל התנאים מתקיימים ולכן הפונקציה היא פונקציית הסתברות טובה
\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
א( 
\begin_inset Formula $\Omega=\left\{ 1,2,3,4,5,6,7,8,9,10\right\} ^{30}$
\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
ב( 
\begin_inset Formula $P_{1}\left(\omega\right)=\frac{\left|\omega\right|}{\left|\Omega\right|}$
\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 נראה שהפונקציה מקיימת את התנאים הנדרשים מפונקציית הסתברות:
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\numeric off
( 
\begin_inset Formula $P_{1}\left(\Omega\right)=\frac{\left|\Omega\right|}{\left|\Omega\right|}=1$
\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\numeric off
( 
\begin_inset Formula $P_{1}$
\end_inset

 מהגדרתה היא חלוקה של
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 שני מספרים אי שליליים, ולכן תוצאתה גם היא חייבת להיות אי שלילית.
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
3
\numeric off
( תהי 
\begin_inset Formula $\left\{ A_{i}\right\} _{i=1}^{\infty}$
\end_inset

 סדרת תתי קבוצות קבוצות זרות של 
\begin_inset Formula $\Omega$
\end_inset

, נקבל:
\begin_inset Formula 
\[
P_{1}\left(\bigcup_{i=1}^{\infty}A_{i}\right)=\frac{\left|\bigcup_{i=1}^{\infty}A_{i}\right|}{\left|\Omega\right|}\stackrel{(*)}{=}\frac{\sum_{i=1}^{\infty}\left|A_{i}\right|}{\left|\Omega\right|}=\sum_{i=1}^{\infty}\frac{\left|A_{i}\right|}{\left|\Omega\right|}=\sum_{i=1}^{\infty}P_{1}\left(A_{i}\right)
\]

\end_inset

)*( גודל איחוד של קבוצות זרות שווה לסכום הגדלים של כל קבוצה
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
ג( נגדיר בה"כ כי הציון של א' הוא הציון הראשון ברשימה, נספור את כמות כל הרשימות
 שבהן האיבר הראשון ברשימה הוא 
\begin_inset Formula $k$
\end_inset

, אם נקבע את האיבר הראשון להיות 
\begin_inset Formula $k$
\end_inset

, יישארו לנו עוד
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
29
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
מקומות ברשימה לקבוע, בלי שום תלות אחד בשני.
 לכן כמות הרשימות כנ"ל תהיה:
\begin_inset Formula 
\[
\left|A_{k}\right|=\left|\left\{ 1,2,3,4,5,6,7,8,9,10\right\} ^{29}\right|=10^{29}
\]

\end_inset

ולכן:
\begin_inset Formula 
\[
P_{1}\left(A_{k}\right)=\frac{\left|A_{k}\right|}{\left|\Omega\right|}=\frac{10^{29}}{10^{30}}=\frac{1}{10}
\]

\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
ד( יהי 
\begin_inset Formula $\omega\in\Omega$
\end_inset

.
\begin_inset Newline newline
\end_inset

אם 
\begin_inset Formula $\omega\in\left\{ \left(k\right)_{i=1}^{30}\mid1\leq k\leq10\right\} =S$
\end_inset

 נגדיר: 
\begin_inset Formula $P_{2}\left(\left\{ \omega\right\} \right)=\frac{1}{10}$
\end_inset


\begin_inset Newline newline
\end_inset

ואם 
\begin_inset Formula $\omega\notin\left\{ \left(k\right)_{i=1}^{30}\mid1\leq k\leq10\right\} =S$
\end_inset

 נגדיר: 
\begin_inset Formula $P_{2}\left(\left\{ \omega\right\} \right)=0$
\end_inset


\begin_inset Newline newline
\end_inset

בכך הגדרנו את 
\begin_inset Formula $P_{2}$
\end_inset

 על כל היחידונים ב-
\begin_inset Formula $\mathcal{F}$
\end_inset

, נראה כי 
\begin_inset Formula $P_{2}$
\end_inset

 היא פונקציית הסתברות חוקית:
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\numeric off
( 
\begin_inset Formula $P_{2}\left(\Omega\right)=1$
\end_inset

:
\begin_inset Formula 
\[
P_{2}\left(\Omega\right)=\sum_{\omega\in\Omega}P_{2}\left(\left\{ \omega\right\} \right)=\sum_{\omega\in S}P_{2}\left(\left\{ \omega\right\} \right)+\sum_{\omega\notin D}P_{2}\left(\left\{ \omega\right\} \right)=10\cdot\frac{1}{10}+10\cdot0=1
\]

\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\numeric off
( לפי הגדרת 
\begin_inset Formula $P_{2}$
\end_inset

: 
\begin_inset Formula $\forall\omega\in\Omega\ P_{2}\left(\left\{ \omega\right\} \right)=\begin{cases}
\frac{1}{10} & \omega\in S\\
0 & \omega\notin S
\end{cases}$
\end_inset

, ובכל מקרה 
\begin_inset Formula $P_{2}\left(\left\{ \omega\right\} \right)\geq0$
\end_inset

 ומאחר ולכל 
\begin_inset Formula $A\in\mathcal{F}$
\end_inset

 מתקיים: 
\begin_inset Formula $P_{2}\left(A\right)=\sum_{\omega\in A}P_{2}\left(\omega\right)$
\end_inset

, זאת אומרת, סכום של איברים אי שליליים, נקבל כי 
\begin_inset Formula $\forall A\in\mathcal{F}\ P_{2}\left(A\right)\geq0$
\end_inset

 
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
\begin_inset space ~
\end_inset

 נראה כעת כי לכל 
\begin_inset Formula $1\leq k\leq10$
\end_inset

 יתקיים 
\begin_inset Formula $P_{2}\left(A_{k}\right)=P_{1}\left(A_{k}\right)$
\end_inset

:
\begin_inset Newline newline
\end_inset

כבר הראינו בסעיף קודם כי 
\begin_inset Formula $P_{1}\left(A_{k}\right)=\frac{1}{10}$
\end_inset

, נותר להראות כי 
\begin_inset Formula $P_{2}\left(A_{k}\right)=\frac{1}{10}$
\end_inset

, ונקבל את השויון הרצוי.
\begin_inset Newline newline
\end_inset

נסמן 
\begin_inset Formula $F=\left\{ \left(a_{i}\right)_{i=1}^{30}\mid a_{1}=k\wedge\forall2\leq i\leq10\ a_{i}\in\left\{ i\right\} _{i=1}^{10}\right\} $
\end_inset


\begin_inset Newline newline
\end_inset

יהי 
\begin_inset Formula $1\leq k\leq10$
\end_inset

, אזי 
\begin_inset Formula $A_{k}=\left\{ \left(k\right)_{i=1}^{10}\right\} \uplus F$
\end_inset

.
 נשים לב כי שתי הקבוצות שמהן מורכב 
\begin_inset Formula $A_{k}$
\end_inset

 הן זרות, כעת נסתכל על 
\begin_inset Formula $P_{2}\left(A_{k}\right)$
\end_inset

:
\begin_inset Formula 
\[
P_{2}\left(A_{k}\right)=P_{2}\left(\left\{ \left(k\right)_{i=1}^{30}\right\} \right)+P_{2}\left(F\right)=\frac{1}{10}+\sum_{\omega\in F}P_{2}\left(\omega\right)=\frac{1}{10}+\sum_{\omega\in F}0=\frac{1}{10}
\]

\end_inset

 ולכן לכל 
\begin_inset Formula $1\leq k\leq10$
\end_inset

:
\begin_inset Formula 
\[
P_{2}\left(A_{k}\right)=P_{1}\left(A_{k}\right)
\]

\end_inset


\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
ה( עבור 
\begin_inset Formula $P_{2}$
\end_inset

, מאחר ולכל מאורע 
\begin_inset Formula $\omega\in\Omega$
\end_inset

 שלא מקיים 
\begin_inset Formula $\omega\in\left\{ \left(k\right)_{i=1}^{10}\mid1\leq k\leq10\right\} $
\end_inset

, נקבל 
\begin_inset Formula $P_{2}\left(\omega\right)=0$
\end_inset

, נסתכל רק על מקרים בהם 
\begin_inset Formula $\omega\in\left\{ \left(k\right)_{i=1}^{10}\mid1\leq k\leq10\right\} $
\end_inset

, ואז נקבל ש-
\begin_inset Formula $P_{2}\left(A_{k}\right)=P_{2}\left(\left\{ \left(k\right)_{i=1}^{10}\right\} \right)=\frac{1}{10}$
\end_inset

, ולכן: 
\begin_inset Formula 
\[
P_{2}\left(A_{7}\right)=\frac{1}{10}
\]

\end_inset


\begin_inset Newline newline
\end_inset

עבור 
\begin_inset Formula $P_{1}$
\end_inset

, נצטרך לספור את כמות האיברים במאורע 
\begin_inset Formula $A_{k}$
\end_inset

, קודם כל נחליט כמה אנשים קיבלנו את הציון המקסימלי 
\begin_inset Formula $k$
\end_inset

, ונסמנו ב-
\begin_inset Formula $i$
\end_inset

, אחרי כן נבחר את המיקומים של האנשים שקיבלו את הציון 
\begin_inset Formula $k$
\end_inset

, יש 
\begin_inset Formula ${30 \choose i}$
\end_inset

 דרכים לבחור את המיקומים האלה.
 כעת נשארו 
\begin_inset Formula $3-i$
\end_inset

 מיקומים, שכל אחד מהם יכול להיות כל מספר בין 
\begin_inset Formula $1$
\end_inset

 ל-
\begin_inset Formula $k-1$
\end_inset

 )אנו דורשים 
\begin_inset Formula $k-1$
\end_inset

 כדי שלא יהיו חפיפות בין הבחירות שלנו(.
 לסיכום, כמות האיברים ב-
\begin_inset Formula $A_{k}$
\end_inset

 תהיה:
\begin_inset Formula 
\[
\left|A_{k}\right|=\sum_{i=1}^{30}{30 \choose i}\cdot\left(k-1\right)^{30-i}
\]

\end_inset

 ועבור 
\begin_inset Formula $A_{7}$
\end_inset

 נקבל: 
\begin_inset Formula $\left|A_{7}\right|=\sum_{i=1}^{30}{30 \choose i}\cdot6^{30-i}$
\end_inset

, כעת אפשר לחשב את 
\begin_inset Formula $P_{1}\left(A_{7}\right)$
\end_inset

:
\begin_inset Formula 
\[
P_{1}\left(A_{7}\right)=\frac{\left|A_{7}\right|}{\left|\Omega\right|}=\frac{\sum_{i=1}^{30}{30 \choose i}\cdot6^{30-i}}{10^{30}}
\]

\end_inset


\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
3
\numeric off
 
\end_layout

\begin_layout Enumerate
נגדיר מרחב מדיד 
\begin_inset Formula $\Omega=\left\{ 1,2,3,4\right\} $
\end_inset

, ופונקציית הסתברות המקיימת לכל 
\begin_inset Formula $\omega\in\Omega$
\end_inset

: 
\begin_inset Formula $P\left(\left\{ \omega\right\} \right)=\frac{1}{4}$
\end_inset

, נראה כי 
\begin_inset Formula $P$
\end_inset

 פונקציית הסתברות חוקית:
\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $\forall A\in\mathcal{F}\ P\left(A\right)\geq0$
\end_inset

:
\begin_inset Newline newline
\end_inset

מהגדרת 
\begin_inset Formula $P$
\end_inset

 לכל יחידון בה ניתן ערך אי שלילי, לכן לכל קבוצה שמכילה כמה יחידונים אנו
 רק נסכום איברים אי שליליים, פעולה הנותנת תוצאה אי שלילית.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $P\left(\Omega\right)=1$
\end_inset

:
\begin_inset Formula 
\[
P\left(\Omega\right)=P\left(\left\{ 1\right\} \cup\left\{ 2\right\} \cup\left\{ 3\right\} \cup\left\{ 4\right\} \right)=\frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\frac{1}{4}=1
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
\begin_inset Argument item:1
status open

\begin_layout Plain Layout

\end_layout

\end_inset

כעת נגדיר את המאורעות 
\begin_inset Formula $A,B,C$
\end_inset

 כך:
\begin_inset Formula 
\[
A=\left\{ 1,2,3\right\} 
\]

\end_inset


\begin_inset Formula 
\[
B=\left\{ 1,3,4\right\} 
\]

\end_inset


\begin_inset Formula 
\[
C=\left\{ 2,3,4\right\} 
\]

\end_inset

לכל אחד מהמאורעות האלה מתקיים:
\begin_inset Formula 
\[
P\left(A\right)=P\left(B\right)=P\left(C\right)=\frac{3}{4}
\]

\end_inset

ולחיתוך של שלושתם נקבל:
\begin_inset Formula 
\[
P\left(A\cap B\cap C\right)=P\left(\left\{ 3\right\} \right)=\frac{1}{4}
\]

\end_inset


\end_layout

\begin_layout Enumerate
נגדיר את המרחב המדיד להיות: 
\begin_inset Formula $\Omega=\left\{ 1,2,3,4,5\right\} $
\end_inset

,ונגדיר את 
\begin_inset Formula $P$
\end_inset

 כך:
\begin_inset Formula 
\[
P\left(\left\{ 1\right\} \right)=0
\]

\end_inset


\begin_inset Formula 
\[
P\left(\left\{ 2\right\} \right)=0
\]

\end_inset


\begin_inset Formula 
\[
P\left(\left\{ 3\right\} \right)=0
\]

\end_inset


\begin_inset Formula 
\[
P\left(\left\{ 4\right\} \right)=\frac{3}{4}
\]

\end_inset


\begin_inset Formula 
\[
P\left(\left\{ 5\right\} \right)=\frac{1}{5}
\]

\end_inset

נראה כי 
\begin_inset Formula $P$
\end_inset

 חוקית:
\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $\forall A\in\mathcal{F}\ P\left(A\right)\geq0$
\end_inset

:
\begin_inset Newline newline
\end_inset

מהגדרת 
\begin_inset Formula $P$
\end_inset

 לכל יחידון בה ניתן ערך אי שלילי, לכן לכל קבוצה שמכילה כמה יחידונים אנו
 רק נסכום איברים אי שליליים, פעולה הנותנת תוצאה אי שלילית.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $P\left(\Omega\right)=1$
\end_inset

:
\begin_inset Formula 
\[
P\left(\Omega\right)=P\left(\left\{ 1\right\} \cup\left\{ 2\right\} \cup\left\{ 3\right\} \cup\left\{ 4\right\} \cup\left\{ 5\right\} \right)=0+0+0+\frac{3}{4}+\frac{1}{4}=1
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
\begin_inset Argument item:1
status open

\begin_layout Plain Layout

\end_layout

\end_inset

כעת נגדיר את המאורעות 
\begin_inset Formula $A,B,C$
\end_inset

 כך:
\begin_inset Formula 
\[
A=\left\{ 1,4\right\} 
\]

\end_inset


\begin_inset Formula 
\[
B=\left\{ 2,4\right\} 
\]

\end_inset


\begin_inset Formula 
\[
C=\left\{ 3,4\right\} 
\]

\end_inset

לכל אחד מהמאורעות האלה מתקיים:
\begin_inset Formula 
\[
P\left(A\right)=P\left(B\right)=P\left(C\right)=\frac{3}{4}
\]

\end_inset

ולחיתוך של שלושתם נקבל:
\begin_inset Formula 
\[
P\left(A\cap B\cap C\right)=P\left(\left\{ 4\right\} \right)=\frac{3}{4}
\]

\end_inset


\end_layout

\begin_layout Enumerate
רשות
\end_layout

\begin_layout Enumerate
עבור מרחב הסתברות כלשהו 
\begin_inset Formula $\left(\Omega,P\right)$
\end_inset

 ושלושה מאורעות 
\begin_inset Formula $A,B,C$
\end_inset

 המקיימים 
\begin_inset Formula $P\left(A\right)=P\left(B\right)=P\left(C\right)=\frac{3}{4}$
\end_inset

, הוכיחו שמתקיים:
\begin_inset Formula 
\[
\frac{1}{4}\leq P\left(A\cap B\cap C\right)\leq\frac{3}{4}
\]

\end_inset

הוכחה:
\begin_inset Newline newline
\end_inset

ע"פ סעיף קודם אנו יודעים שמתקיים:
\begin_inset Formula 
\[
\left(A\cap B\cap C\right)^{c}=\left(A^{c}\cup B^{c}\cup C^{c}\right)
\]

\end_inset

ומכאן:
\begin_inset Formula 
\[
A\cap B\cap C=\left(A^{c}\cup B^{c}\cup C^{c}\right)^{c}
\]

\end_inset

לכן:
\begin_inset Formula 
\[
P\left(A\cap B\cap C\right)=P\left(\left(A^{c}\cup B^{c}\cup C^{c}\right)^{c}\right)=1-P\left(A^{c}\cup B^{c}\cup C^{c}\right)
\]

\end_inset

 כעת נחסום את הביטוי הימני, נשים לב שהוא יהיה מינימלי כאשר המשלימים של שלושת
 הקבוצות יהיו זרים )זאת אומרת: 
\begin_inset Formula $\forall S\neq T\in\left\{ A,B,C\right\} \ S\cap T=\phi$
\end_inset

( ואז עדיין נקבל:
\begin_inset Formula 
\[
1-P\left(A^{c}\cup B^{c}\cup C^{c}\right)=1-P\left(A^{c}\right)-P\left(B^{c}\right)-P\left(C^{c}\right)=1-\left(1-\frac{3}{4}\right)-\left(1-\frac{3}{4}\right)-\left(1-\frac{3}{4}\right)=\frac{1}{4}
\]

\end_inset

נשים לב גם שהוא יהיה מקסימלי כאשר 
\begin_inset Formula $A=B=C$
\end_inset

 ואז נקבל: 
\begin_inset Formula 
\[
P\left(A^{c}\cup B^{c}\cup C^{c}\right)=P\left(A^{c}\right)=P\left(B^{c}\right)=P\left(C^{c}\right)=\frac{1}{4}
\]

\end_inset


\begin_inset Formula 
\[
1-P\left(A^{c}\cup B^{c}\cup C^{c}\right)=1-\frac{1}{4}=\frac{3}{4}
\]

\end_inset

בכל מקרה של חיתוך מסוים בין הקבוצות נקבל 
\begin_inset Formula $\frac{1}{4}\leq P\left(A^{c}\cup B^{c}\cup C^{c}\right)\leq\frac{3}{4}$
\end_inset

, ובכל מקרה:
\begin_inset Formula 
\[
\frac{1}{4}\leq1-P\left(A^{c}\cup B^{c}\cup C^{c}\right)\leq\frac{3}{4}
\]

\end_inset

ולכן:
\begin_inset Formula 
\[
\frac{1}{4}\leq P\left(A\cap B\cap C\right)\leq\frac{3}{4}
\]

\end_inset


\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
4
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\Omega=\left\{ \left(i,t\right)\mid1\leq i\leq13,t\in\left\{ s,h,c,d\right\} \right\} ^{13}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\forall\omega\in\Omega\ P\left(\left\{ \omega\right\} \right)=\frac{1}{\left|\Omega\right|}=\frac{1}{52^{13}}$
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $\forall A\in\mathcal{F}\ P\left(A\right)\geq0$
\end_inset

:
\begin_inset Newline newline
\end_inset

מהגדרת 
\begin_inset Formula $P$
\end_inset

 לכל יחידון בה ניתן ערך אי שלילי, לכן לכל קבוצה שמכילה כמה יחידונים אנו
 רק נסכום איברים אי שליליים, פעולה הנותנת תוצאה אי שלילית.
\end_layout

\begin_layout Enumerate
\begin_inset Formula $P\left(\Omega\right)=1$
\end_inset

:
\begin_inset Formula 
\[
P\left(\Omega\right)=P\left(\bigcup_{\omega\in\Omega}\left\{ \omega\right\} \right)=\sum_{\omega\in\Omega}P\left(\left\{ \omega\right\} \right)=\left(13\cdot4\right)^{13}\cdot\frac{1}{52^{13}}=1
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
\begin_inset Argument item:1
status open

\begin_layout Plain Layout

\end_layout

\end_inset

 מדובר על סדרה באורך
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
13
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
כך שכל איבר בה הוא זוג מהצורה 
\begin_inset Formula $\left(i,t\right)$
\end_inset

,נבחר את ארבעת המקומות שבהם יש קלף מסוג לב: 
\begin_inset Formula ${13 \choose 4}$
\end_inset

, נבחר אילו קלפי לב יהיו שם: 
\begin_inset Formula $\frac{13!}{9!}$
\end_inset

, כל שאר הקלפים ייבחרו באקראי: 
\begin_inset Formula $\frac{\left(52-4\right)!}{\left(52-4-9\right)!}=\frac{48!}{39!}$
\end_inset

, ונקבל כי כמות המאורעות שמכילים בדיוק
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
4
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
קלפי לב היא: 
\begin_inset Formula ${13 \choose 4}\cdot\frac{13!}{9!}\cdot\frac{48!}{39!}$
\end_inset

, נחלק ב
\begin_inset Formula $\left|\Omega\right|$
\end_inset

 ונקבל: 
\begin_inset Formula $P\left(H_{4}\right)=\frac{{13 \choose 4}\cdot\frac{13!}{9!}\cdot\frac{48!}{39!}}{52^{13}}$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $\Omega=\left\{ i\mid1\leq i\leq12\right\} ^{5}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\forall\omega\in\Omega\ P\left(\left\{ \omega\right\} \right)=\frac{1}{12^{5}}$
\end_inset


\begin_inset Newline newline
\end_inset

ניתן לראות ש-
\begin_inset Formula $P$
\end_inset

 חוקית בדיוק מאותן סיבות כמו בסעיף הקודם, עד כדי שינוי המספרים.
\begin_inset Newline newline
\end_inset

נבחר חודש:
\begin_inset Formula ${12 \choose 1}$
\end_inset

, נבחר את המיקומים של האנשים שנולדו בחודש זה: 
\begin_inset Formula ${5 \choose 2}$
\end_inset

, ושאר האנשים ייולדו בחודש אקראי: 
\begin_inset Formula $12^{3}$
\end_inset

 ונקבל כי ההסתברות שלפחות שניים מהם נולדו באותו החודש היא: 
\begin_inset Formula $\frac{{12 \choose 1}\cdot{5 \choose 2}12^{3}}{\left|\Omega\right|}=\frac{{5 \choose 2}\cdot12^{4}}{12^{5}}=\frac{10}{12}$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $\Omega=\left\{ 0,1\right\} ^{16}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\forall\omega\in\Omega\ P\left(\left\{ \omega\right\} \right)=\frac{1}{2^{16}}$
\end_inset


\begin_inset Newline newline
\end_inset

בהינתן המין של הבנאדם שיושב ראשון, יש בדיוק דרך אחת לסדר את שאר האנשים,
 ישנן
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 שתי אפשרויות לבחור את המין של הבנאדם הראשון ולכן ההסתברות שהם ישבו לסירוגין
 היא: 
\begin_inset Formula $\frac{2}{\left|\Omega\right|}=\frac{1}{2^{15}}$
\end_inset


\end_layout

\begin_layout Enumerate
ניתן לכל סופר מספר בין
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
ל-
\numeric on
20
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
ונסדר אותם בשורה, כך שעשרת הראשונים הם בקבוצה הראשונה והאחרונים הם בקבוצה
 השנייה, נקבל:
\begin_inset Newline newline
\end_inset


\begin_inset Formula $\Omega=\left\{ a_{i}\mid1\leq i\leq20,\,a_{i}\in\left\{ i\right\} _{i=1}^{20},\,\forall j\neq k\ a_{j}\neq a_{k}\right\} $
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\forall\omega\in\Omega\ P\left(\left\{ \omega\right\} \right)=\frac{1}{\left|\Omega\right|}=\frac{1}{20!}$
\end_inset


\begin_inset Newline newline
\end_inset

נבחר את הקבוצה שאליה משתייכים מאיר, צרויה וענר, יש
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
אפשרויות לכך, אחר כך נבחר את המיקומים שלהם, יש: 
\begin_inset Formula $\frac{10!}{7!}$
\end_inset

 אפשרויות לכך, ולבסוף נבחר את כל הסידורים האפשריים לשאר הסופרים, יש: 
\begin_inset Formula $17!$
\end_inset

 אפשרויות לכך.
 נקבל כי ההסתברות שמאיר צרויה וענר יהיו באותה קבוצה היא: 
\begin_inset Formula $\frac{2\cdot\frac{10!}{7!}\cdot17!}{20!}=\frac{\cancel{2\cdot10}\cdot9\cdot8}{\cancel{20}\cdot19\cdot18}=\frac{9\cdot8}{19\cdot18}$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $\Omega=\left\{ \left(x_{i}\right)_{i=1}^{10}\mid x_{i}\in\mathbb{N}\cup\left\{ 0\right\} ,\,\sum_{i=1}^{10}x_{i}=15\right\} $
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\forall\omega\in\Omega\ P\left(\left\{ \omega\right\} \right)=\frac{1}{\left|\Omega\right|}=\frac{1}{{15+10-1 \choose 10-1}}=\frac{1}{{24 \choose 9}}$
\end_inset


\begin_inset Newline newline
\end_inset

נבחר את שלושת התאים שיישארו ריקים: 
\begin_inset Formula ${10 \choose 3}$
\end_inset

, כעת, משאנחנו יודעים אילו תאים יישארו ריקים נותר רק לחלק את
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
15
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
הכדורים בין
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
שבעת התאים הנותרים, ובנוסף נדרוש שבכל תאיהיה לפחות כדור אחד, אז נחלק לכל
 תא כדור, ואז נספור את כמות הדרכים לחלק את
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 שמונת 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
הכדורים הנותרים לשבעת התאים הנותרים, יש 
\begin_inset Formula ${8+7-1 \choose 7-1}={14 \choose 6}$
\end_inset

 אפשרויות לכך, ונקבל כי ההסתברות שיישארו בדיוק
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 שלושה תאים ריקים היא: 
\begin_inset Formula $\frac{{10 \choose 3}\cdot{14 \choose 6}}{{24 \choose 9}}$
\end_inset


\end_layout

\end_body
\end_document
