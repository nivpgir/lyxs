#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass heb-article
\begin_preamble
\usepackage{mathbbm}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\topmargin 2cm
\rightmargin 1cm
\secnumdepth -2
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation 0bp
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
הסתברות - תרגיל
\family roman
\series medium
\shape up
\size largest
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
9
\end_layout

\begin_layout Author
ניב פגיר -
\family roman
\series medium
\shape up
\size large
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
305328692
\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "../ProperSetup.lyx"

\end_inset


\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
1
\end_layout

\begin_layout Standard
\begin_inset Formula $X,Y,Z$
\end_inset

 מ"מ דיסקרטיים מוגדרים מעל אותו מ"ה
\end_layout

\begin_layout Enumerate
צ"ל: 
\begin_inset Formula $\Expec{X+Y\mid Z}=\Expec{X\mid Z}+\Expec{Y\mid Z}$
\end_inset

, נראה:
\begin_inset Formula 
\begin{align*}
\Expec{X+Y\mid Z}\left(\omega\right) & =\Expec{X+Y\mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} }\\
 & =\sum_{\omega_{2}\in\Omega}\left(X+Y\right)\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} \right)\\
 & =\sum_{\omega_{2}\in\Omega}\left(X\left(\omega_{2}\right)+Y\left(\omega_{2}\right)\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} \right)\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} \right)+Y\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} \right)\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} \right)+\sum_{\omega_{2}\in\Omega}Y\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} \right)\\
 & =\Expec{X\mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} }+\Expec{Y\mid\left\{ \omega_{1}\mid Z\left(\omega\right)=Z\left(\omega_{1}\right)\right\} }\\
 & =\Expec{X\mid Z}+\Expec{Y\mid Z}
\end{align*}

\end_inset


\end_layout

\begin_layout Enumerate
נחשב:
\begin_inset Formula 
\begin{align*}
\Expec{X\mid X} & =\Expec{X\mid\left\{ \omega_{1}\mid X\left(\omega\right)=X\left(\omega_{1}\right)\right\} }\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid X\left(\omega\right)=X\left(\omega_{1}\right)\right\} \right)
\end{align*}

\end_inset

נסמן: 
\begin_inset Formula $A=\left\{ \omega_{1}\mid X\left(\omega\right)=X\left(\omega_{1}\right)\right\} $
\end_inset

 ונקבל:
\begin_inset Formula 
\begin{align*}
\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid X\left(\omega\right)=X\left(\omega_{1}\right)\right\} \right) & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid A\right)\\
\\
\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid A\right) & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot\frac{P\left(\left\{ \omega_{2}\right\} \right)\cdot1_{A}\left(\omega_{2}\right)}{P\left(A\right)}\\
 & =\frac{1}{P\left(A\right)}\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \right)\cdot1_{A}\left(\omega_{2}\right)\\
 & =\frac{1}{P\left(A\right)}\sum_{\omega_{2}\in A}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \right)\\
 & \overnum =1\frac{1}{P\left(A\right)}\sum_{\omega_{2}\in A}X\left(\omega\right)\cdot P\left(\left\{ \omega_{2}\right\} \right)\\
 & =\frac{X\left(\omega\right)}{P\left(A\right)}\cdot\sum_{\omega_{2}\in A}P\left(\left\{ \omega_{2}\right\} \right)\\
 & \overnum =2\frac{X\left(\omega\right)}{\cancel{P\left(A\right)}}\cancel{P\left(A\right)}\\
 & =X\left(\omega\right)
\end{align*}

\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\left(1\right)$
\end_inset

כי 
\begin_inset Formula $\omega_{2}\in A\iff X\left(\omega_{2}\right)=X\left(\omega\right)$
\end_inset

 ע"פ הגדרת 
\begin_inset Formula $A$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $\left(2\right)$
\end_inset

כי אנחנו סוכמים על כל היחידונים של 
\begin_inset Formula $A$
\end_inset

, וסכום הסתברויות של קבוצות זרות שווה להסתברות של איחוד הקבוצות.
\end_layout

\begin_layout Enumerate
נחשב:
\begin_inset Formula 
\begin{align*}
\Expec{X\mid Y}\left(\omega\right) & =\Expec{X\mid\left\{ \omega_{1}\mid Y\left(\omega\right)=Y\left(\omega_{1}\right)\right\} }=\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid\left\{ \omega_{1}\mid Y\left(\omega\right)=Y\left(\omega_{1}\right)\right\} \right)
\end{align*}

\end_inset

כעת, לפי מה שאני מבין, לפי התשובה לשאלה לגבי הסתברות מותנית מהפורום, אם
 
\begin_inset Formula $Y\left(\omega\right)\neq c$
\end_inset

, אז 
\begin_inset Formula $P\left(\left\{ \omega_{1}\mid Y\left(\omega\right)=Y\left(\omega_{1}\right)\right\} \right)=0$
\end_inset

 )כי 
\begin_inset Formula $P\left(Y=c\right)=1$
\end_inset

 וגם 
\begin_inset Formula $1=P\left(\Omega\right)=P\left(Y=c\right)+P\left(Y\neq c\right)$
\end_inset

(, ואז כל הביטוי הנ"ל אינו מוגדר כלל.
 לכן מעתה נניח כי 
\begin_inset Formula $Y\left(\omega\right)=c$
\end_inset

, ואז נקבל:
\begin_inset Formula 
\begin{align*}
\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid Y\left(\omega\right)=Y\right) & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \mid Y=c\right)\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot\frac{P\left(\left\{ \omega_{2}\right\} \cap\left\{ \omega_{1}\mid c=Y\left(\omega_{1}\right)\right\} \right)}{P\left(Y=c\right)}\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \cap\left\{ \omega_{1}\mid c=Y\left(\omega_{1}\right)\right\} \right)\\
 & =\sum_{\omega_{2}\in\Omega}X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \right)\cdot1_{\left\{ \omega_{1}\mid c=Y\left(\omega_{1}\right)\right\} }\left(\omega_{2}\right)\\
 & =\sum_{\omega_{2}\in\left\{ \omega_{1}\mid c=Y\left(\omega_{1}\right)\right\} }X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \right)
\end{align*}

\end_inset

קיבלנו כי המשתנה המקרי כלל אינו מוגדר על כל מרחב המדגם, לכן בעיקרון הוא
 כלל אינו משתנה מקרי תקין, אבל אם נעמיד פנים שהוא משתנה מקרי תקין, נקבל:
\begin_inset Formula 
\[
P_{\Expec{X\mid Y}}\left(k\right)=\begin{cases}
1 & k=\sum_{\omega_{2}\in\left\{ \omega_{1}\mid c=Y\left(\omega_{1}\right)\right\} }X\left(\omega_{2}\right)\cdot P\left(\left\{ \omega_{2}\right\} \right)\\
0 & else
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Enumerate
ראשית נניח כי 
\begin_inset Formula $P\left(Y=Y\left(\omega\right)\right)>0$
\end_inset

, אחרת כל הביטוי לא מוגדר טוב.
 ועכשיו קצת חישובים על 
\begin_inset Formula $\Expec{X\mid Y}$
\end_inset

:
\begin_inset Formula 
\begin{align*}
\Expec{X\mid Y}\left(\omega\right) & =\Expec{X\mid\left\{ \omega_{1}\mid Y\left(\omega\right)=Y\left(\omega_{1}\right)\right\} }=\\
 & =\sum_{s\in\im X}s\cdot P\left(X=s\mid Y\left(\omega\right)=Y\right)\\
 & =c\cdot P\left(X=c\mid Y\left(\omega\right)=Y\right)+\sum_{c\neq s\in\im X}s\cdot\underbrace{P\left(X=s\mid Y\left(\omega\right)=Y\right)}_{=0}\\
 & =c\cdot P\left(X=c\mid Y\left(\omega\right)=Y\right)\\
 & =c\cdot\frac{P\left(X=c\land Y\left(\omega\right)=Y\right)}{c\cdot P\left(Y\left(\omega\right)=Y\right)}\\
 & =c\cdot\frac{P\left(X^{-1}\left(c\right)\cap Y^{-1}\left(Y\left(\omega\right)\right)\right)}{c\cdot P\left(Y^{-1}\left(Y\left(\omega\right)\right)\right)}
\end{align*}

\end_inset

עכשיו נראה כי 
\begin_inset Formula $P\left(Y^{-1}\left(Y\left(\omega\right)\right)\right)=P\left(X^{-1}\left(c\right)\cap Y^{-1}\left(Y\left(\omega\right)\right)\right)$
\end_inset

:
\begin_inset Formula 
\begin{align*}
P\left(Y^{-1}\left(Y\left(\omega\right)\right)\right) & =P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\Omega\right)\\
 & =P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left(\left\{ \omega\mid X\left(\omega\right)=c\right\} \uplus\left\{ \omega\mid X\left(\omega\right)\neq c\right\} \right)\right)\\
 & =P\left(\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left\{ \omega\mid X\left(\omega\right)=c\right\} \right)\uplus\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left\{ \omega\mid X\left(\omega\right)\neq c\right\} \right)\right)\\
 & =P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left\{ \omega\mid X\left(\omega\right)=c\right\} \right)+P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left\{ \omega\mid X\left(\omega\right)\neq c\right\} \right)\\
 & \leq P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left\{ \omega\mid X\left(\omega\right)=c\right\} \right)+\underbrace{P\left(\left\{ \omega\mid X\left(\omega\right)\neq c\right\} \right)}_{=0}\\
 & =P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap\left\{ \omega\mid X\left(\omega\right)=c\right\} \right)\\
 & =P\left(Y^{-1}\left(Y\left(\omega\right)\right)\cap X^{-1}\left(c\right)\right)
\end{align*}

\end_inset

לכן למעשה נקבל כי: 
\begin_inset Formula 
\[
\Expec{X\mid Y}\left(\omega\right)=c
\]

\end_inset

ואז בעצם:
\begin_inset Formula 
\begin{align*}
p_{\Expec{X\mid Y}}\left(k\right) & =P\left(\Expec{X\mid Y}=k\right)=\begin{cases}
1 & k=c\\
0 & else
\end{cases}
\end{align*}

\end_inset


\end_layout

\begin_layout Enumerate
נחשב: 
\begin_inset Formula 
\begin{align*}
\Expec{X\mid Y}\left(\omega\right) & =\sum_{s\in\im X}s\cdot P\left(X=s\mid Y=Y\left(\omega\right)\right)\\
 & =\sum_{s\in\im X}s\cdot P\left(X=s\right)P\left(Y=Y\left(\omega\right)\right)\\
 & =P\left(Y=Y\left(\omega\right)\right)\sum_{s\in\im X}s\cdot P\left(X=s\right)\\
 & =P\left(Y=Y\left(\omega\right)\right)\Expec X
\end{align*}

\end_inset


\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
2
\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
3
\end_layout

\begin_layout Standard
רשות
\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
4
\end_layout

\begin_layout Section
שאלה
\family roman
\series bold
\shape up
\size larger
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit
5
\end_layout

\end_body
\end_document
